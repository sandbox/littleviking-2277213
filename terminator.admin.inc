<?php

/**
 * @file
 * Admin form builder functions for the Terminator module.
 */

/**
 * Form builder for terminator tagging form.
 */
function terminator_form($form, $form_state, $vocabulary) {
  $tree = taxonomy_get_tree($vocabulary->vid);
  $form = array(
    '#theme' => 'terminator_form',
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'terminator') . '/terminator.css'),
      'js' => array(drupal_get_path('module', 'terminator') . '/terminator.js'),
      'library' => array(
        array('system', 'ui.draggable'),
        array('system', 'ui.droppable'),
      ),
    ),
    '#tree' => TRUE,
  );
  $form['vocabulary'] = array(
    '#type' => 'value',
    '#value' => $vocabulary,
  );
  $form['terms'] = array();
  foreach ($tree as $term) {
    // In its unthemed state, this form is a series of textfields - one for each
    // term in the tree. The fields accept a comma-separated list of nids to add
    // the respective term to.
    $form['terms'][$term->tid] = array(
      '#type' => 'textfield',
      '#title' => check_plain($term->name),
      '#prefix' => str_repeat('<div class="indentation">&nbsp;</div>', $term->depth),
    );
  }
  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search for content'),
    '#description' => t('Enter the title of the node, and drag it onto a term to tag it.'),
    '#ajax' => array(
      'callback' => 'terminator_node_list',
      'wrapper' => 'terminator-node-list',
      'method' => 'replace',
      'effect' => 'none',
      'event' => 'keyup',
    ),
  );
  $node_list = array();
  if (!empty($form_state['values'])) {
    if ($string = trim($form_state['values']['search'])) {
      $result = db_select('node')
        ->fields('node', array('nid', 'title'))
        ->condition('title', '%' . db_like($string) . '%', 'LIKE')
        ->range(0, 20)
        ->execute();
      foreach ($result as $row) {
        $node_list[] = '
          <div class="terminator-draggable draggable" data-nid="' . $row->nid . '">
            <a class="tabledrag-handle" href="#"><div class="handle">&nbsp;</div></a>
            ' . $row->title . '
          </div>
        ';
      }
    }
  }
  $form['node_list'] = array(
    '#prefix' => '<div id="terminator-node-list">',
    '#markup' => implode('', $node_list),
    '#suffix' => '</div>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );
  return $form;
}

/**
 * Validate terminator form input.
 */
function terminator_form_validate($form, &$form_state) {
  $form_state['values']['entity_fields'] = array();
  $vocabulary = $form_state['values']['vocabulary'];
  $terms = array_filter($form_state['values']['terms']);
  foreach ($terms as $tid => $nids_string) {
    // Each term field accepts a comma-separated list of nids to add itself to.
    // Break up that list into an array of nids.
    $nids = array_map('trim', explode(',', $nids_string));
    foreach ($nids as $nid) {
      // Check that node has a term ref field that can accept this term.
      $node = node_load($nid);
      $valid_fields = array();
      $fields = _terminator_get_entity_taxonomy_fields('node', $node->type, $node);
      foreach ($fields as $field_name => $field_info) {
        foreach ($field_info['field']['settings']['allowed_values'] as $allowed_value) {
          if ($allowed_value['vocabulary'] == $vocabulary->machine_name) {
            $valid_fields[$field_name] = $field_name;
          }
        }
      }
      if (empty($valid_fields)) {
        // Node does not have a reference for this vocabulary.
        form_set_error(
          'terms][' . $tid,
          t('%vocabulary terms cannot be applied to %entity.',
            array('%vocabulary' => $vocabulary->name, '%entity' => $node->title)
          )
        );
      }
      elseif (count($valid_fields) > 1) {
        // Node has more than one ref field for this vocabulary.
        form_set_error(
          'terms][' . $tid,
          t('%vocabulary field is ambiguous on %entity.',
            array('%vocabulary' => $vocabulary->name, '%entity' => $node->title)
          )
        );
        // @todo Give users a way to resolve multiple fields accept the same
        // vocabulary.
      }
      else {
        // Exactly one valid field - success. Save field name to $form_state so
        // the submit handles doesn't have to repeat this logic.
        $form_state['values']['entity_field_name'][$node->nid] = reset($valid_fields);
      }
    }
  }
}

/**
 * Handle terminator form submit.
 */
function terminator_form_submit($form, &$form_state) {
  foreach (array_filter($form_state['values']['terms']) as $tid => $nids_string) {
    $nids = array_map('trim', explode(',', $nids_string));
    foreach ($nids as $nid) {
      // Assign tid to node.
      $node = node_load($nid);
      if ($field_name = $form_state['values']['entity_field_name'][$node->nid]) {
        $node->{$field_name}[$node->language][] = array('tid' => $tid);
        node_save($node);
      }
    }
  }
  drupal_set_message(t('All changes have been saved.'));
}
