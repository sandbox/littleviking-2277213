(function($) {
  Drupal.behaviors.terminatorInitialize = {
    attach: function(context, settings) {
      $('.terminator-form tr', context).once('terminator').each(function() {
        renderRow($(this));
      });
    }
  };
  Drupal.behaviors.terminatorAddRemove = {
    attach: function(context, settings) {
      // drag and drop to add node to term
      $('.terminator-draggable', context).once('terminator-draggable').draggable({
        revert: true
      });
      $('.terminator-form tr', context).once('terminator-droppable').droppable({
        tolerance: 'pointer',
        drop: function(event, ui) {
          terminatorAddNode($(this), $(ui.draggable).data('nid'), $(ui.draggable).text());
        }
      });

      // click remove to remove node from list
      $('.terminator-remove', context).once('terminator-remove').click(function(e) {
        e.preventDefault();
        terminatorRemoveNode($(this).closest('.terminator-form tr'), $(this).closest('li').data('nid'));
      });
    }
  };

  /**
   * Add a node to a term. Term data is stored as a data attribute on the <tr>
   * @param  Object $term_tr <tr> as a jQuery object
   * @param  String nid
   * @param  String title
   */
  function terminatorAddNode($term_tr, nid, title) {
    var terms = $term_tr.data('terms') || {};
    terms[nid] = title;
    $term_tr.data('terms', terms);
    renderRow($term_tr);
  }

  /**
   * Remove a node from a term. Term data is stored as data-terms on the <tr>
   * @param  Object $term_tr <tr> as a jQuery object
   * @param  String nid
   */
  function terminatorRemoveNode($term_tr, nid) {
    var terms = $term_tr.data('terms') || {};
    delete terms[nid];
    $term_tr.data('terms', terms);
    renderRow($term_tr);
  }

  /**
   * Force re-render of a <tr> with data-terms metadata.
   * @param  Object $term_tr <tr> as a jQuery object
   */
  function renderRow($term_tr) {
    var terms = $term_tr.data('terms') || {};
    var nids = Object.keys(terms);
    var $title_list = nids.reduce(function($output, nid) {
      // Process the terms array into an unordered list of titles
      $output.append($('<li>').data('nid', nid).html(terms[nid].trim() + ' <a href="#" class="terminator-remove">' + Drupal.t('remove') + '</a>'));
      return $output;
    }, $('<ul>'));
    $('input', $term_tr).val(nids.join(','));
    $('ul', $term_tr).replaceWith($title_list);
    Drupal.attachBehaviors($term_tr);
  }
})(jQuery);
