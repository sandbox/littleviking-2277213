<?php

/**
 * @file
 * Two column layout for terminator form.
 *
 * Available variables:
 * - $form: The entire terminator form.
 * - $term_tree: The selected taxonomy vocabulary rendered as a table. The table
 *   also contains hidden form fields to populate during drag-and-drop.
 */

?>

<div class="terminator-form">
  <div class="messages warning"><?php print t('Changes made in this table will not be saved until the form is submitted.') ?></div>
  <div class="row">
    <div class="col half-width">
      <?php print $term_tree ?>
    </div>
    <div class="col half-width">
      <?php print render($form['search']) ?>
      <?php print render($form['node_list']) ?>
    </div>
  </div>
  <?php print drupal_render_children($form) ?>
</div>
