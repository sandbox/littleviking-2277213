INTRODUCTION
------------
Terminator provides a convenient interface for adding different taxonomy terms
to multiple nodes at once. The terminator interface is presented in two columns.
One column lists the full tree of a vocabulary, and the second column
has a node search field. Admins search for nodes by title, then drag and
drop desired nodes onto the taxonomy tree for quick tagging.

REQUIREMENTS
------------
This module requires the following modules:
* Taxonomy

CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:
  - Use terminator tagging
    The terminator form is only available to users with this permission.
